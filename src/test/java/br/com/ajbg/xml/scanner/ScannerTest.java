package br.com.ajbg.xml.scanner;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.com.ajbg.xml.parser.GrammarSymbol;

public class ScannerTest {
	@Test
	public void wellFormedFile() {
		Scanner scan = new Scanner(Scanner.class.getResourceAsStream(
				"/example1.sdx"));
		assert scan.getNextToken().getKind() == GrammarSymbol.OPN_DEF;
		assert scan.getNextToken().getKind() == GrammarSymbol.XML;
		assert scan.getNextToken().getKind() == GrammarSymbol.ID;
		assert scan.getNextToken().getKind() == GrammarSymbol.IGUAL;
		assert scan.getNextToken().getKind() == GrammarSymbol.LINHA;
		assert scan.getNextToken().getKind() == GrammarSymbol.ID;
		assert scan.getNextToken().getKind() == GrammarSymbol.IGUAL;
		assert scan.getNextToken().getKind() == GrammarSymbol.LINHA;
		assert scan.getNextToken().getKind() == GrammarSymbol.CLS_DEF;

		assert scan.getNextToken().getKind() == GrammarSymbol.MEQ;
		assert scan.getNextToken().getKind() == GrammarSymbol.ID;
		assert scan.getNextToken().getKind() == GrammarSymbol.ID;
		assert scan.getNextToken().getKind() == GrammarSymbol.IGUAL;
		assert scan.getNextToken().getKind() == GrammarSymbol.LINHA;
		assert scan.getNextToken().getKind() == GrammarSymbol.MAQ;

		assert scan.getNextToken().getKind() == GrammarSymbol.MEQ;
		assert scan.getNextToken().getKind() == GrammarSymbol.ID;
		assert scan.getNextToken().getKind() == GrammarSymbol.ID;
		assert scan.getNextToken().getKind() == GrammarSymbol.TEXTO;
		assert scan.getNextToken().getKind() == GrammarSymbol.ID;
		assert scan.getNextToken().getKind() == GrammarSymbol.MAQ;

		assert scan.getNextToken().getKind() == GrammarSymbol.FECHA;
		assert scan.getNextToken().getKind() == GrammarSymbol.ID;
		assert scan.getNextToken().getKind() == GrammarSymbol.MAQ;
	}

	@Test(expected=LexicalException.class)
	public void badFormedFile() {
		Scanner scan = new Scanner(Scanner.class.getResourceAsStream(
				"/example2.sdx"));
		assert scan.getNextToken().getKind() == GrammarSymbol.OPN_DEF;
		assert scan.getNextToken().getKind() == GrammarSymbol.XML;
		assert scan.getNextToken().getKind() == GrammarSymbol.ID;
		assert scan.getNextToken().getKind() == GrammarSymbol.IGUAL;
		assert scan.getNextToken().getKind() == GrammarSymbol.LINHA;
		assert scan.getNextToken().getKind() == GrammarSymbol.ID;
		assert scan.getNextToken().getKind() == GrammarSymbol.IGUAL;
		assert scan.getNextToken().getKind() == GrammarSymbol.LINHA;
		assert scan.getNextToken().getKind() == GrammarSymbol.CLS_DEF;

		assert scan.getNextToken().getKind() == GrammarSymbol.MEQ;
		assert scan.getNextToken().getKind() == GrammarSymbol.ID;
		assert scan.getNextToken().getKind() == GrammarSymbol.ID;
		assert scan.getNextToken().getKind() == GrammarSymbol.IGUAL;
		assert scan.getNextToken().getKind() == GrammarSymbol.LINHA;
		assert scan.getNextToken().getKind() == GrammarSymbol.FECHA;
	}
}
