package br.com.ajbg.xml.scanner;

/**
 * Lexical Exception
 * @version 2010-september-04
 * @discipline Compiladores
 * @author Gustavo H P Carvalho
 * @email gustavohpcarvalho@ecomp.poli.br
 */
public class LexicalException extends RuntimeException {

	private static final long serialVersionUID = 3457448332803077642L;
	
	// Not expected char
	private char character;
	// Line and column of error
	private int line, column;
	
	/**
	 * Default constructor
	 * @param message
	 * @param c
	 * @param line
	 * @param column
	 */
	public LexicalException(String message, char c, int line, int column) {
		super(message);
		this.character = c;
		this.line = line;
		this.column = column;
	}
	
	/**
	 * Creates the error report
	 */	
	public String toString() {
		String errorMessage = super.getMessage() + ": " + this.character + "("
				+ this.line + ", " + this.column + ")";
			
		return errorMessage;
	}
	
}	
