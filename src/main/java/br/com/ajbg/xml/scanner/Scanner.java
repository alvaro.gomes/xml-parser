package br.com.ajbg.xml.scanner;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import br.com.ajbg.xml.parser.GrammarSymbol;

public class Scanner {
	private InputStream file;

	private char currentChar;
	private int line, column;

	private void getNextChar() {
		if (currentChar == '\n') {
			line++;
			column = 1;
		} else {
			if (currentChar == '\t') {
				column += 3;
			}
			column++;
		}
		try {
			currentChar = (char) file.read();
		} catch (IOException e) {
			throw new RuntimeException("Erro lendo arquivo!", e);
		}
	}

	public Scanner(InputStream file) {
		this.line = 1;
		this.column = 0;
		this.file = file;
		getNextChar();
	}

	private boolean isSeparator(char c) {
		return (c == ' ' || c == '\n' || c == '\t');
	}

	private boolean isEOF(char c) {
		return (byte)c == -1;
	}

	private boolean isDigit(char c) {
		return (c >= '0' && c <= '9');
	}

	private boolean isSmallLetter(char c) {
		return (c >= 'a' && c <= 'z');
	}

	private boolean isCapitalLetter(char c) {
		return (c >= 'A' && c <= 'Z');
	}

	private boolean isLetter(char c) {
		return (isSmallLetter(c) || isCapitalLetter(c));
	}

	private boolean isForbbidenLine(char c) {
		return c == '"';
	}

	private boolean isForbbidenTexto(char c) {
		return c == '<';
	}

	private GrammarSymbol scanToken(StringBuilder currentSpelling) {
		GrammarSymbol state = GrammarSymbol.EMPTY;
		while(true) {
			switch (state) {
			case EMPTY:
				currentSpelling.append(currentChar);
				if (isSmallLetter(currentChar)) {
					state = GrammarSymbol.ID;
					getNextChar();
				} else if (currentChar == '>') {
					state = GrammarSymbol.TEXTO;
					getNextChar();
				} else if (currentChar == '<') {
					state = GrammarSymbol.FECHA;
					getNextChar();
				} else if (currentChar == '=') {
					state = GrammarSymbol.IGUAL;
					getNextChar();
				} else if (currentChar == '"') {
					state = GrammarSymbol.LINHA;
					getNextChar();
				} else if (currentChar == '?') {
					state = GrammarSymbol.CLS_DEF;
					getNextChar();
				} else {
					state = GrammarSymbol.ERR;
				}
				break;

			case ID:
				while (isLetter(currentChar) || isDigit(currentChar)
						|| currentChar == '-') {
					currentSpelling.append(currentChar);
					getNextChar();
				}
				if (currentSpelling.toString().equals("xml")) {
					return GrammarSymbol.XML;
				} else {
					return GrammarSymbol.ID;
				}

			case FECHA:
				if (currentChar == '/') {
					currentSpelling.append(currentChar);
					getNextChar();
					return GrammarSymbol.FECHA;
				} else if (currentChar == '?') {
					currentSpelling.append(currentChar);
					getNextChar();
					return GrammarSymbol.OPN_DEF;
				} else {
					return GrammarSymbol.MEQ;
				}

			case TEXTO:
				while (isSeparator(currentChar)) {
					getNextChar();
				}
				if (isForbbidenTexto(currentChar) || isEOF(currentChar)) {
					return GrammarSymbol.MAQ;
				} else {
					while (!isForbbidenTexto(currentChar)) {
						currentSpelling.append(currentChar);
						getNextChar();
					}
					currentSpelling.append(currentChar);
					getNextChar();
					if (currentChar == '/') {
						currentSpelling.append(currentChar);
						getNextChar();
						return GrammarSymbol.TEXTO;
					} else {
						state = GrammarSymbol.ERR;
						break;
					}
				}

			case IGUAL:
				return GrammarSymbol.IGUAL;

			case CLS_DEF:
				if (currentChar == '>') {
					currentSpelling.append(currentChar);
					getNextChar();
					return GrammarSymbol.CLS_DEF;
				} else {
					state = GrammarSymbol.ERR;
					break;
				}

			case LINHA:
				while (!isForbbidenLine(currentChar)) {
					currentSpelling.append(currentChar);
					getNextChar();
				}
				currentSpelling.append(currentChar);
				getNextChar();
				return GrammarSymbol.LINHA;

			case ERR:

			default:
				throw new LexicalException("ERRO LEXICO",
						currentChar, line, column);
			}
		}
	}

	public Token getNextToken() {
		while (isSeparator(currentChar)) {
			getNextChar();
		}

		StringBuilder currentSpelling = new StringBuilder();
		int line = this.line;
		int column = this.column;
		GrammarSymbol currKind = scanToken(currentSpelling);

		return new Token(currKind, currentSpelling.toString(), line, column);
	}
}
