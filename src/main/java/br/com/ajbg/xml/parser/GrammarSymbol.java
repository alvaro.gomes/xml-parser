package br.com.ajbg.xml.parser;

public enum GrammarSymbol {

	EMPTY,		// Estado inicial
	ID,			// [a-z][a-zA-Z1-9-]*
	MAQ,		// >
	MEQ,		// <
	FECHA,		// </
	OPN_DEF,	// '<?'
	CLS_DEF,	// '?>'
	IGUAL,		// =
	LINHA,		// '"' (^")* '"'
	TEXTO,		// MAQ (^<)+ FECHA
	XML,		// 'xml'
	ERR;		// Estado de erro
}
